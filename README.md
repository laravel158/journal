## Справочник журналов

Просмотр журналов и авторов. С возможностью CRUD

#### Установка
1. Склонировать проект **journal**: `git clone https://gitlab.com/laravel158/journal.git`
2. Перейти в проект: `cp journal`
3. Файл .env.example скопировать и переименовать в .env. В нем указать доступы к БД

#### Запуск
	1. Установить Composer зависимости: `php composer install`
	2. Сгенерировать ключ: `php artisan key:generate`
	3. Запустить в БД миграции: `php artisan migrate`
	4. Запустить заполнение данными БД: 
        - Згружаем список журналов и авторов в БД
            `php artisan db:seed --class=DatabaseSeeder`
        - Згружаем связь журналов с авторами в БД
            `php artisan db:seed --class=JournalAuthorsSeeder`
    5. Скомпилировать проект: npm run watch    	
    7. Назначить прада для загрузки картинок: chmod -R 755 storage/app/public/images/
    8. Создать символическую ссылку:
        `php artisan storage:link`
	8. Запустить сервер: `php artisan serve --port=8000`

#### Описание
- Просмотр отдельно страниц Журналов и Авторов
- На странице Авторов можно увидеть список всех журналов определенного автора
- Сортировка(с использованием ajax) Авторов по № и Фамилии 
- Сортировка(с использованием ajax) Журнолов по дате выпуска
- Пагинация(с использованием ajax) по журналам и авторам 

#### Визуализация
![Alt text](public/screen.png?raw=true "promotion")
