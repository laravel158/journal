<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required|string|max:255|min:3',
            'firstname' => 'required|string|max:255',
            'secondname' => 'string'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'lastname' => 'Фамилию',
            'firstname' => 'Имя',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required'  => 'Необходимо заполнить :attribute.',
            'string'    => ':attribute должен быть строкой',
            'min'       => ':attribute должен состоять не менее чем из 3 символов',
        ];
    }
}
