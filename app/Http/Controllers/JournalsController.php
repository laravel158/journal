<?php

namespace App\Http\Controllers;

use App\Http\Requests\JournalRequest;
use App\Models\Author;
use App\Models\Image;
use App\Models\Journal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class JournalsController extends Controller
{
    const PAGINATE_DEFAULT = 5;

    public function index()
    {
        $journals = Journal::orderBy('created_at', 'asc')->paginate(self::PAGINATE_DEFAULT);

        return view('journals.index', ['journals' => $journals]);
    }

    public function create()
    {
        $authors = Author::all();

        return view('journals.create', ['authors' => $authors]);
    }

    public function store(JournalRequest $request)
    {
        $credentials    = $request->only('title', 'description', 'image', 'authors');
        $imagePath      = Storage::disk('public')->put('/images', $credentials['image']);

        $journal = new Journal($credentials);
        $journal->save();

        foreach($credentials['authors'] as $author) {
            $journal->authors()->attach($author);
        }

        $image = new Image();

        $image->path        = $imagePath;
        $image->journal_id  = $journal->id;
        $image->save();

        return redirect()->route('journals');
    }

    public function show(Journal $journal)
    {
        return view('journals.show', ['journal' => $journal]);
    }

    public function edit(Journal $journal)
    {
        $authors = Author::all();

        return view('journals.edit', ['journal' => $journal, 'authors' => $authors]);
    }

    public function update(JournalRequest $request,  Journal $journal)
    {
        $credentials = $request->only('title', 'description', 'image', 'authors');
        $imagePath   = Storage::disk('public')->put('/images', $credentials['image']);

        $image = new Image();

        $image->path        = $imagePath;
        $image->journal_id  = $journal->id;
        $image->save();

        $journal->authors()->sync($credentials['authors']);
        $journal->update($credentials);

        return redirect()->route('journals');
    }

    public function delete(Journal $journal)
    {
        $journal->authors()->detach();
        $journal->delete();

        return redirect()->route('journals');
    }

    public function fetchData(Request $request)
    {
        if ($request->ajax()) {
            $sortBy     = $request->post('sort_by');
            $sortType   = $request->post('sort_type');

            $journals = Journal::orderBy($sortBy, $sortType)->paginate(self::PAGINATE_DEFAULT);

            return view('journals.journal_data', ['journals' => $journals])->render();
        }
    }
}
