<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorRequest;
use App\Models\Author;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
    const PAGINATE_DEFAULT = 5;

    public function index()
    {
        $authors = Author::orderBy('id', 'asc')->paginate(self::PAGINATE_DEFAULT);

        return view('authors.index', ['authors' => $authors]);
    }

    public function create()
    {
        return view('authors.create');
    }

    public function store(AuthorRequest $request)
    {
        $credentials = $request->only('lastname', 'firstname', 'secondname');

        (new Author($credentials))->save();

        return redirect()->route('authors');
    }

    public function show(Author $author)
    {
        return view('authors.show', ['author' => $author]);
    }

    public function edit(Author $author)
    {
        return view('authors.edit', ['author' => $author]);
    }

    public function update(AuthorRequest $request,  Author $author)
    {
        $credentials = $request->only('lastname', 'firstname', 'secondname');

        $author->update($credentials);

        return redirect()->route('authors');
    }

    public function delete(Author $author)
    {
        $author->delete();

        return redirect()->route('authors');
    }

    public function journals(Author $author)
    {
        $journals = $author->journals()->get();

        return view('authors.journals', ['author' => $author, 'journals' => $journals]);
    }

    public function fetchData(Request $request)
    {
        if ($request->ajax()) {
            $sortBy     = $request->post('sort_by');
            $sortType   = $request->post('sort_type');

            $authors = Author::orderBy($sortBy, $sortType)->paginate(self::PAGINATE_DEFAULT);

            return view('authors.author_data', ['authors' => $authors])->render();
        }
    }
}
