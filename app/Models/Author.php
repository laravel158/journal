<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'authors';
    protected $guarded = false;
    protected $fillable = [
        'lastname',
        'firstname',
        'secondname',
    ];

    public function journals()
    {
        return $this->belongsToMany(Journal::class, 'journal_authors', 'author_id', 'journal_id');
    }
}
