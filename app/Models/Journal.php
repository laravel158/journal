<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Journal extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'journals';
    protected $guarded = false;
    protected $fillable = [
        'title',
        'description',
    ];

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class, 'journal_authors', 'journal_id', 'author_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return  Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}
