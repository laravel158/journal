<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalAuthorsTable extends Migration
{
    private $table = 'journal_authors';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('journal_id');
            $table->unsignedBigInteger('author_id');

            $table->timestamps();
            $table->softDeletes();

            // IDX
            $table->index('journal_id', 'journal_author_journal_idx');
            $table->index('author_id', 'journal_author_author_idx');

            // FK
            $table->foreign('journal_id', 'journal_author_journal_fk')->on('journals')->references('id');
            $table->foreign('author_id', 'journal_author_author_fk')->on('authors')->references('id');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
