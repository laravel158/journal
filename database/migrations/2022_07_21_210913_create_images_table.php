<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    private $table = 'images';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->string('path')->nullable();
            $table->string('url')->nullable();
            $table->unsignedBigInteger('journal_id');
            $table->timestamps();
            $table->softDeletes();

            // IDX
            $table->index('journal_id', 'image_journal_idx');

            // FK
            $table->foreign('journal_id', 'image_journal_fk')->on('journals')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
