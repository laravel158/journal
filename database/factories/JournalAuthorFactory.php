<?php

namespace Database\Factories;

use App\Models\JournalAuthor;
use Illuminate\Database\Eloquent\Factories\Factory;

class JournalAuthorFactory extends Factory
{
    protected $model = JournalAuthor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        return [
            'journal_id' => random_int(1, 10),
            'author_id' => random_int(1, 10),
        ];
    }
}
