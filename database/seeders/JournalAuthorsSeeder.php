<?php

namespace Database\Seeders;

use App\Models\JournalAuthor;
use Illuminate\Database\Seeder;

class JournalAuthorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JournalAuthor::factory(10)->create();
    }
}
