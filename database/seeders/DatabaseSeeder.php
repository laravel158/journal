<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Journal;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Journal::factory(10)->create();
        Author::factory(10)->create();
    }
}
