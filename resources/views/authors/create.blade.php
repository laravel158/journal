@extends('layout')

@section('content')
    <div class="d-flex">
        <div class="w-100">
            <h3>Добавление автора</h3>
        </div>
    </div>
    <form action="{{ route('authors.store') }}" method="POST">
        @csrf
        <div class="mb-3 mt-3">
            <label for="lastname" class="form-label">Фамилия</label>
            <input type="text"
                   class="form-control @error('lastname') is-invalid @enderror"
                   id="title"
                   placeholder="Введите фамилию"
                   name="lastname"
                   value="{{ old('lastname') }}"
            />
            @error('lastname')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3 mt-3">
            <label for="firstname" class="form-label">Имя</label>
            <input type="text"
                   class="form-control @error('firstname') is-invalid @enderror"
                   id="title"
                   placeholder="Введите имя"
                   name="firstname"
                   value="{{ old('firstname') }}"
            />
            @error('firstname')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3 mt-3">
            <label for="secondname" class="form-label">Отчество</label>
            <input type="text"
                   class="form-control"
                   id="secondname"
                   placeholder="Введите отчество"
                   name="secondname"
                   value="{{ old('secondname') }}"
            />
        </div>
        <button type="submit" class="btn btn-primary">Создать</button>
        <a href="{{ route('authors') }}" class="btn btn-default">Назад</a>
    </form>
@endsection
