@extends('layout')

@section('content')
    <div class="container mt-3">
        <h2>Список авторов</h2>
        <a href="{{ route('authors.create') }}" class="btn btn-block btn-primary">Добавить</a>

        @if(!empty($authors) && $authors->count())
            @csrf
            <table class="table table-striped" id="authors-table">
                <thead>
                <tr>
                    <th class="sorting" data-sorting_type="asc" data-column_name="id">№ <span class="px-2" id="id_icon"></span></th>
                    <th class="sorting" data-sorting_type="asc" data-column_name="lastname">Фамилия <span class="px-2" id="lastname_icon"></span></th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @include('authors.author_data')
                </tbody>
            </table>
            <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
            <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id" />
            <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
        @else
            <p>Данные не найдены</p>
        @endif
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            function fetchData(page, sort_type, sort_by) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').val()
                    },
                    url: "{{ route('authors.fetch_data') }}",
                    method: 'post',
                    data: {page: page, sort_by: sort_by, sort_type: sort_type},
                    success: function(data) {
                        let _tbody = '#authors-table tbody';

                        $(_tbody).html('');
                        $(_tbody).html(data);
                    }
                });
            }

            $(document).on('click', '.sorting', function(event) {
                event.preventDefault();

                var _columnName = $(this).data('column_name');
                var _orderType = $(this).data('sorting_type');
                var _reverseOrder = '';

                if (_orderType === 'asc') {
                    $(this).data('sorting_type', 'desc');

                    _reverseOrder = 'desc';

                    $('#' + _columnName + '_icon').html('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">\n' +
                        '  <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>\n' +
                        '</svg>');
                }

                if (_orderType === 'desc') {
                    $(this).data('sorting_type', 'asc');

                    _reverseOrder = 'asc';

                    $('#' + _columnName + '_icon').html('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">\n' +
                        '  <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>\n' +
                        '</svg>');
                }

                $('#hidden_column_name').val(_columnName);
                $('#hidden_sort_type').val(_reverseOrder);

                var _page = $('#hidden_page').val();

                fetchData(_page, _reverseOrder, _columnName);
            });

            $(document).on('click', '.pagination li a.page-link', function(event) {
                event.preventDefault();

                var _page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(_page);

                var _columnName = $('#hidden_column_name').val();
                var _sortType = $('#hidden_sort_type').val();

                fetchData(_page, _sortType, _columnName);
            });
        });
    </script>
@endsection
<style>
    .sorting {cursor:default;}
</style>
