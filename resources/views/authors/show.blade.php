@extends('layout')

@section('content')
    <table class="table">
        <tbody>
            <tr>
                <th scope="row">Фамилия</th><td>{{ $author->lastname }}</td>
            </tr>
            <tr>
                <th scope="row">Имя</th><td>{{ $author->firstname }}</td>
            </tr>
            <tr>
                <th scope="row">Отчество</th><td>{{ $author->secondname }}</td>
            </tr>
        </tbody>
    </table>
@endsection
