@extends('layout')

@section('content')
    <div class="container mt-3">
        <div class="d-flex">
            <div class="w-100">
                <h2>Список журналов - {{ $author->lastname }} {{ $author->firstname }}</h2>
            </div>
            <div class="flex-shrink-1">
                <a href="{{ route('authors') }}" class="text-md text-gray-700 underline px-4">Назад</a>
            </div>
        </div>
        @if(!empty($journals) && $journals->count())
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Короткое описание</th>
                    <th>Картинка</th>
                    <th>Авторы</th>
                    <th>Дата выпуска</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($journals as $journal)
                    <tr>
                        <td>{{ $journal->id }}</td>
                        <td>{{ $journal->title }}</td>
                        <td>{{ $journal->description }}</td>
                        <td>
                            @if (isset($journal->images->first()->path) )
                                <img src="{{ url('storage/' . $journal->images->first()->path) }}" class="img-fluid img-thumbnail w-25 p-3" style="width:50px;" />
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @foreach($journal->authors as $author)
                                <p>{{ $author->lastname }} {{ $author->firstname }}</p>
                            @endforeach
                        </td>
                        <td>{{ $journal->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>Данные не найдены</p>
        @endif
    </div>
@endsection
