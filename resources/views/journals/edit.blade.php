@extends('layout')

@section('content')
    <div class="d-flex">
        <div class="w-100">
            <h3>Редактирование журнала</h3>
        </div>
    </div>
    <form action="{{ route('journals.update', $journal->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="mb-3 mt-3">
            <label for="title" class="form-label">Название</label>
            <input type="text"
                   class="form-control @error('title') is-invalid @enderror"
                   id="title"
                   placeholder="Введите название"
                   name="title"
                   value="{{ $journal->title }}"
            />
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Короткое описание</label>
            <textarea class="form-control" rows="5" id="description" name="description">{{ $journal->description }}</textarea>
        </div>
        <div class="mb-3">
            @if (isset($journal->images->first()->path) )
                <img src="{{ url('storage/' . $journal->images->first()->path) }}" class="img-fluid img-thumbnail w-25 p-3" style="width:50px;" />
            @endif
        </div>
        <div class="mb-3">
            <lable class="form-label">Выберите изображение</lable>
            <input type="file" class="form-control" name="image" />
        </div>
        <div class="mb-3">
            <label for="authors" class="form-label">Выберите автора</label>
            <select name="authors[]" id="authors" class="form-control" multiple>
                <option value="">- - Выберите автора - -</option>
                @foreach($authors as $author)
                    <option
                        value="{{ $author->id }}"
                        {{ is_array($journal->authors->pluck('id')->toArray()) && in_array($author->id, $journal->authors->pluck('id')->toArray()) ? ' selected' : '' }}
                    >
                        {{ $author->lastname }} {{$author->firstname }}
                    </option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Обновить</button>
        <a href="{{ route('journals') }}" class="btn btn-default">Назад</a>
    </form>
@endsection
