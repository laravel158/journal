@extends('layout')

@section('content')
    <div class="d-flex">
        <div class="w-100">
            <h3>Добавление журнала</h3>
        </div>
    </div>
    <form action="{{ route('journals.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-3 mt-3">
            <label for="title" class="form-label">Название</label>
            <input type="text"
                   class="form-control @error('title') is-invalid @enderror"
                   id="title"
                   placeholder="Введите название"
                   name="title"
                   value="{{ old('title') }}"
            />
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Короткое описание</label>
            <textarea class="form-control" rows="5" id="description" name="description">{{ old('description') }}</textarea>
        </div>
        <div class="mb-3">
            <lable class="form-label">Выберите изображение</lable>
            <input type="file" class="form-control" name="image" />
        </div>
        <div class="mb-3">
            <label for="authors" class="form-label">Выберите авторов</label>
            <select name="authors[]" id="authors" class="form-control" multiple>
                @foreach($authors as $author)
                    <option
                        {{ is_array(old('authors')) && in_array($author->id, old('authors')) ? ' selected' : '' }}
                        value="{{ $author->id }}"
                    >
                        {{ $author->lastname }} {{$author->firstname }}
                    </option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Создать</button>
        <a href="{{ route('journals') }}" class="btn btn-default">Назад</a>
    </form>
@endsection
