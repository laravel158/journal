@extends('layout')

@section('content')
    <table class="table">
        <tbody>
            <tr>
                <th scope="row">Название</th><td>{{ $journal->title }}</td>
            </tr>
            <tr>
                <th scope="row">Короткое описание</th><td>{{ $journal->description }}</td>
            </tr>
            <tr>
                <th scope="row">Катинка</th>
                <td>
                    @if (isset($journal->images->first()->path) )
                        <img src="{{ url('storage/' . $journal->images->first()->path) }}" class="img-fluid img-thumbnail w-25 p-3" style="width:50px;" />
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <th scope="row">Авторы</th>
                <td>
                    @foreach($journal->authors as $author)
                        <p>{{ $author->lastname }} {{ $author->firstname }}</p>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th scope="row">Дата выпуска</th><td>{{ $journal->created_at }}</td>
            </tr>
        </tbody>
    </table>
@endsection
