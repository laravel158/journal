<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\JournalsController::class, 'index'])->name('main');

Route::group(['namespace' => 'Journals', 'prefix' => 'journals'], function(){
    Route::get('/', [\App\Http\Controllers\JournalsController::class, 'index'])->name('journals');
    Route::get('/create', [\App\Http\Controllers\JournalsController::class, 'create'])->name('journals.create');
    Route::post('/', [\App\Http\Controllers\JournalsController::class, 'store'])->name('journals.store');
    Route::get('/{journal}', [\App\Http\Controllers\JournalsController::class, 'show'])->name('journals.show');
    Route::get('/edit/{journal}', [\App\Http\Controllers\JournalsController::class, 'edit'])->name('journals.edit');
    Route::patch('/{journal}', [\App\Http\Controllers\JournalsController::class, 'update'])->name('journals.update');
    Route::delete('/{journal}', [\App\Http\Controllers\JournalsController::class, 'delete'])->name('journals.delete');
    Route::post('/fetch_data', [\App\Http\Controllers\JournalsController::class, 'fetchData'])->name('journals.fetch_data');
});

Route::group(['namespace' => 'Authors', 'prefix' => 'authors'], function(){
    Route::get('/', [\App\Http\Controllers\AuthorsController::class, 'index'])->name('authors');
    Route::get('/create', [\App\Http\Controllers\AuthorsController::class, 'create'])->name('authors.create');
    Route::post('/', [\App\Http\Controllers\AuthorsController::class, 'store'])->name('authors.store');
    Route::get('/{author}', [\App\Http\Controllers\AuthorsController::class, 'show'])->name('authors.show');
    Route::get('/edit/{author}', [\App\Http\Controllers\AuthorsController::class, 'edit'])->name('authors.edit');
    Route::patch('/{author}', [\App\Http\Controllers\AuthorsController::class, 'update'])->name('authors.update');
    Route::delete('/{author}', [\App\Http\Controllers\AuthorsController::class, 'delete'])->name('authors.delete');
    Route::get('/{author}/journals', [\App\Http\Controllers\AuthorsController::class, 'journals'])->name('authors.journals');
    Route::post('/fetch_data', [\App\Http\Controllers\AuthorsController::class, 'fetchData'])->name('authors.fetch_data');
});
